module ServiceOrdersHelper

  def validate_service_order(service_order)
    validate_status(service_order)
    validate_devices_statuses(service_order)

  end

  def validate_status(service_order)
    case service_order.so_status_type_fk
      when SoStatusType::VASTU_VOETUD
        # No validation needed
      else
        service_order.service_devices.each do |device|
          if device.service_device_status_type_fk == ServiceDeviceStatusType::VASTU_VOETUD
            service_order.errors.add(:base, 'Pöördumise seisundit ei saa muuta sest kõik seadmed pole valmis seisundis')
            return
          end
        end
        service_order.service_actions.each do |service_action|
          if service_action.service_action_status_type_fk == ServiceActionStatusType::POOLELI
            service_order.errors.add(:base, 'Pöördumise seisundit ei saa muuta sest kõik tööd pole veel valmis')
            return
          end
        end
    end
  end

  def validate_devices_statuses(service_order)
    service_order.service_devices.each do |device|
      if device.service_device_status_type_fk != ServiceDeviceStatusType::VASTU_VOETUD
        service_order.service_actions.each do |sa|
          if sa.service_device_fk == device.service_device && sa.service_action_status_type_fk == ServiceActionStatusType::POOLELI
            service_order.errors.add(:base, 'Seadme seisundit ei saa muuta sest sellega seotud tööd pole veel lõpetatud')
            return
          end
        end

      end
    end

  end
end
