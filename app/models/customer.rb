class Customer < ActiveRecord::Base
  self.table_name = 'customer'
  self.primary_key = :customer

  belongs_to :person, -> { where 'customer.subject_type_fk' => 1 }, foreign_key: 'subject_fk'
  belongs_to :enterprise, -> { where 'customer.subject_type_fk' => 2 }, foreign_key: 'subject_fk'

  PERSON = 1
  ENTERPRISE = 2

  def name
    if is_person?
      self.person.first_name + ' ' +self.person.last_name
    elsif is_enterprise?
      self.enterprise.name
    else
      ''
    end
  end

  def is_person?
    subject_type_fk == PERSON
  end

  def is_enterprise?
    subject_type_fk == ENTERPRISE
  end

  def self.join_sql
    ' LEFT JOIN "person" ON "person"."person" = "customer"."subject_fk" AND "customer"."subject_type_fk" = 1
  LEFT JOIN "enterprise" ON "enterprise"."enterprise" = "customer"."subject_fk" AND "customer"."subject_type_fk" = 2'
  end

  def get_name
    if sub_object.nil?
      return ''
    end
    if is_person?
      sub_object.first_name + ' ' +sub_object.last_name
    elsif is_enterprise?
      sub_object.name
    end
  end

  def sub_object
    unless @sub_object.nil?
      return @sub_object
    end
    if is_person?
      @sub_object = Person.find_by(person: subject_fk)
    elsif is_enterprise?
      @sub_object = Enterprise.find_by(enterprise: subject_fk)
    end
  end
end
