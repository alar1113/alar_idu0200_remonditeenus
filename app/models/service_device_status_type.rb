class ServiceDeviceStatusType < ActiveRecord::Base
    self.table_name = 'service_device_status_type'
    self.primary_key = :service_device_status_type

    VASTU_VOETUD = 1
    TOO_SEADMEGA_LOPETATUD = 2
    SEADE_KLIENDILE_TAGASTATUD = 3
end
