class ServiceRequest < ActiveRecord::Base
  self.table_name = 'service_request'
  self.primary_key = :service_request

  has_many :service_orders, foreign_key: 'service_request_fk'
  belongs_to :customer, foreign_key: 'customer_fk'
  belongs_to :employee, foreign_key: 'created_by'
  belongs_to :service_request_status_type, foreign_key: 'service_request_status_type_fk'

  def deletable?
    self.service_orders.count == 0
  end
end
