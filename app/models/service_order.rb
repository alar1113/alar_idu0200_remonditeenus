class ServiceOrder < ActiveRecord::Base
  self.table_name = 'service_order'
  self.primary_key = :service_order
  include ServiceOrdersHelper

  has_many :service_parts, foreign_key: 'service_order_fk'
  has_many :service_actions, foreign_key: 'service_order_fk'
  has_many :service_devices, foreign_key: 'service_order_fk'
  belongs_to :so_status_type, foreign_key: 'so_status_type_fk'
  belongs_to :creator, foreign_key: 'created_by', :class_name => "Employee"
  belongs_to :updater, foreign_key: 'updated_by', :class_name => "Employee"
  belongs_to :status_changer, foreign_key: 'status_changed_by', :class_name => "Employee"
  belongs_to :service_request, foreign_key: 'service_request_fk'

  accepts_nested_attributes_for :service_devices
  accepts_nested_attributes_for :service_actions, :allow_destroy => true
  accepts_nested_attributes_for :service_parts, :allow_destroy => true

  before_save :calculate_prices


  def status_changer_name
    if status_changed_by.nil?
      ''
    else
      status_changer.name
    end
  end

  def updater_name
    if updated_by.nil?
      ''
    else
      updater.name
    end
  end

  def calculate_prices
    total_price = 0
    service_actions.each do |action|
      sum = action.service_amount.to_f * action.price.to_f
      total_price += sum
    end
    service_parts.each do |part|
      sum = part.part_count.to_f * part.part_price.to_f
      total_price += sum
    end
    self.price_total = total_price
  end

  def edit_sub_records?
    so_status_type_fk == SoStatusType::VASTU_VOETUD
  end

  def deletable?
    self.service_devices.count == 0 && self.service_actions.count == 0 && self.service_parts.count == 0
  end

  validate do |service_order|
    validate_service_order(service_order)
  end

end
