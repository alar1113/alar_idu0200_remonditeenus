class DeviceType < ActiveRecord::Base
    self.table_name = 'device_type'
    self.primary_key = :device_type

    def sub_types
      DeviceType.where(level:2, super_type_fk:self.device_type)
    end

    def label
      self.type_name
    end

end
