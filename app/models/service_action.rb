class ServiceAction < ActiveRecord::Base
  self.table_name = 'service_action'
  self.primary_key = :service_action
  belongs_to :service_type, foreign_key: 'service_type_fk'
  belongs_to :service_action_status_type, foreign_key: 'service_action_status_type_fk'
  belongs_to :service_order, foreign_key: 'service_order_fk'
  belongs_to :service_device, foreign_key: 'service_device_fk'


  attr_accessor :_destroy

  validates_presence_of :service_amount

  def price_total
    if !service_amount.blank? && !price.blank?
      '%.2f' % (service_amount.to_f * price.to_f)
    else
      ''
    end
  end

  def _destroy=(val)
    if val == '1'
      mark_for_destruction
    end
  end
end
