class UserAccount < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable

  self.table_name = 'user_account'
  self.primary_key = :user_account

  def encrypted_password
    passw
  end

  def encrypted_password=(value)
    @passw = value
  end

  def valid_password?(password)
    self.passw == Digest::MD5.hexdigest(password) ||
        self.passw == password
  end


  def employee_id
    if subject_type_fk == 3
      return subject_fk
    end
    nil
  end
end
