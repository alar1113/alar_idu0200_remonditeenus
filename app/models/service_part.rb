class ServicePart < ActiveRecord::Base
    self.table_name = 'service_part'
    self.primary_key = :service_part

    belongs_to :service_device, foreign_key: 'service_device_fk'

    attr_accessor :_destroy
    validates_numericality_of :part_count
    validate :check_serial
    validates_presence_of :part_price
    validates_numericality_of :part_price

    def _destroy=(val)
      if val == '1'
        mark_for_destruction
      end
    end

    def check_serial
      if !serial_no.blank? && part_count > 1
        errors.add(:part_count, 'peab olema 1 kui seeria nr on sisestatud')
      end
    end
end
