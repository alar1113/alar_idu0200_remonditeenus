class ServiceDevice < ActiveRecord::Base
  self.table_name = 'service_device'
  self.primary_key = :service_device
  belongs_to :device, foreign_key: 'device_fk'
  belongs_to :service_order, foreign_key: 'service_order_fk'
  has_many :service_parts, foreign_key: 'service_device_fk'
  has_many :service_actions, foreign_key: 'service_device_fk'

  attr_accessor :_destroy

  def name
    "#{device.manufacturer} #{device.model} #{device.name} "
  end

  def _destroy=(val)
    if val == '1'
      if self.service_actions.size > 0 || self.service_parts.size > 0
      else
        mark_for_destruction
      end
    end
  end

  def valid_to_delete?
    self.service_actions.where(service_order_fk: self.service_order_fk).size == 0 &&
        self.service_parts.where(service_order_fk: self.service_order_fk).size == 0
  end

end
