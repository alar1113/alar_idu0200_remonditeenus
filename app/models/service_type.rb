class ServiceType < ActiveRecord::Base
    self.table_name = 'service_type'
    self.primary_key = :service_type
    belongs_to :service_unit_type, foreign_key: 'service_unit_type_fk'

end
