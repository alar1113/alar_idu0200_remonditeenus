class ServiceActionStatusType < ActiveRecord::Base
  self.table_name = 'service_action_status_type'
  self.primary_key = :service_action_status_type
  POOLELI = 1
  VALMIS = 2

end
