class Device < ActiveRecord::Base
  self.table_name = 'device'
  self.primary_key = :device
  belongs_to :device_type, foreign_key: 'device_type_fk'
  has_many :service_devices, foreign_key: 'device_fk'

  validates_presence_of :name
  validates_length_of :reg_no, maximum: 100
  validates_format_of :manufacturer, :with => /\A[A-Za-z ]+\Z/, message: 'Ainult tähed on lubatud'

end
