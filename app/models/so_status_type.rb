class SoStatusType < ActiveRecord::Base
    self.table_name = 'so_status_type'
    self.primary_key = :so_status_type

    VASTU_VOETUD = 1
    VALMIS = 2
    HINNASTATUD = 3
    ARVE_TEHTUD = 4
    SEADE_TAGASTATUD = 5

end
