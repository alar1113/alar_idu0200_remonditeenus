json.array!(@service_requests) do |service_request|
  json.extract! service_request, :id, :service_desc_by_customer, :service_desc_by_employee, :customer_fk, :service_request_status_type_fk
  json.url service_request_url(service_request, format: :json)
end
