class DevicesController < ApplicationController
  before_action :set_device, only: [:show, :edit, :update, :destroy]
  require 'will_paginate/array'

  respond_to :html

  def index
    @devices = Device.includes(:device_type).order('device_type.type_name').paginate(:page => params[:page], :per_page => 25)
    respond_with(@devices)
  end

  def show
    respond_with(@device)
  end

  def new
    @device = Device.new
    respond_with(@device)
  end

  def edit
  end

  def create
    @device = Device.new(device_params)
    if @device.save
      if params[:service_order] && params[:service_order][:id]
        @service_order = ServiceOrder.find_by(service_order: params[:service_order][:id].to_i)
        unless @service_order.nil?
          ServiceDevice.create(service_order_fk: @service_order.service_order, device_fk: @device.device, service_device_status_type_fk: 1)
          redirect_to edit_service_request_service_order_path(@service_order.service_request, @service_order)
          flash[:info] = 'Uus toode lisatud'
          return
        end
      end
      redirect_to edit_device_path(@device), notice: 'Seadme andmed salvestatud'
    else
      render action: 'new'
    end
  end

  def update
    if @device.update(device_params)
      redirect_to edit_device_path(@device), notice: 'Seadme andmed salvestatud'
    else
      render action: 'edit'
    end
  end

  def destroy
    if @device.service_devices.size > 0
      flash[:error] = 'Seadet ei saa kustutada, kuna see on seotud juba mõne tellimusega'
    else
      @device.destroy
    end
    redirect_to devices_path
  end

  def search
    @device_search = DeviceSearch.new
    if request.post?
      if get_where_clause.size == 0
        flash.now[:error] = 'Sisestage vähemalt 1 otsingu parameeter'
        return
      end
      @devices = Device.find_by_sql([get_search_select, *get_params]).paginate(:page => params[:page], :per_page => 25)
      @device_search = DeviceSearch.new(params.require(:device_search).permit!)
    end
  end

  def add_device_to_order
    if params[:service_order] && params[:device]
      @service_order = ServiceOrder.find_by(service_order: params[:service_order].to_i)
      unless @service_order.nil?
        ServiceDevice.create(service_order_fk: @service_order.service_order, device_fk: params[:device].to_i, service_device_status_type_fk: 1)
        redirect_to edit_service_request_service_order_path(@service_order.service_request, @service_order)
        flash[:info] = 'Uus toode lisatud'
        return
      end
    end
    redirect_to search_devices_path
  end

  private
  def set_device
    @device = Device.find(params[:id])
  end

  def device_params
    params.require(:device).permit(:device, :device_type_fk, :name, :reg_no, :description, :model, :manufacturer, :service_order_id)
  end

  def get_params
    [
        name: "%#{params[:device_search][:name]}%",
        reg_no: "%#{params[:device_search][:reg_no]}%",
        description: "%#{params[:device_search][:description]}%",
        model: "%#{params[:device_search][:model]}%",
        manufacturer: "%#{params[:device_search][:manufacturer]}%",
        customer: "%#{params[:device_search][:client_name]}%",
        device_type_fk: params[:device_search][:device_type_fk].blank? ? nil : params[:device_search][:device_type_fk].to_i
    ]
  end

  def get_search_select
    sql = []
    where = get_where_clause
    sql << 'SELECT d.* FROM device d'
    unless params[:device_search][:client_name].blank?
      sql << 'LEFT JOIN service_device sd ON sd.device_fk = d.device'
      sql << 'LEFT JOIN service_order so ON sd.service_order_fk = so.service_order'
      sql << 'LEFT JOIN service_request sr ON so.service_request_fk = sr.service_request'
      sql << 'LEFT JOIN customer c ON sr.customer_fk = c.customer'
      sql << 'LEFT JOIN person p ON c.subject_fk = p.person AND c.subject_type_fk = 1'
      sql << 'LEFT JOIN enterprise e ON c.subject_fk = e.enterprise AND c.subject_type_fk = 2'
    end
    if where.size > 0
      sql << 'WHERE'
      sql << where.join(' AND ')
    end
    sql.join(' ')
  end

  def get_where_clause
    where = []
    where << 'd.name ILIKE :name' if has_device_param?(:name)
    where << 'd.reg_no ILIKE :reg_no' if has_device_param?(:reg_no)
    where << 'd.description ILIKE :description' if has_device_param?(:description)
    where << 'd.model ILIKE :model' if has_device_param?(:model)
    where << 'd.manufacturer ILIKE :manufacturer' if has_device_param?(:manufacturer)
    where << 'd.device_type_fk = :device_type_fk' if has_device_param?(:device_type_fk)
    unless params[:device_search][:client_name].blank?
      where << 'p.last_name ILIKE :customer OR e.name ILIKE :customer'
    end
    where
  end

  def has_device_param?(name)
    !params[:device_search][name].blank?
  end

end


class String
  def numeric?
    Float(self) != nil rescue false
  end
end
