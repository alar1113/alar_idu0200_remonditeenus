class CustomerController < ApplicationController
  def find_customer
    search_term = "#{params[:term]}%"
    customers = Customer.select("customer.customer AS value, COALESCE(person.first_name || ' ' || person.last_name, enterprise.full_name) AS label").joins(Customer.join_sql).where('person.last_name ilike :search or enterprise.full_name ilike :search', search: search_term)
    render json: customers.to_json
  end
end
