class ServiceTypesController < ApplicationController
  def get_default_price
    result = {}
    service_type_id = params[:service_type_id].to_i
    service_type = ServiceType.find_by(service_type: service_type_id)
    unless service_type.nil?
      result[:price] = service_type.service_price
      result[:unit_type] = service_type.service_unit_type.type_name
    end
    render json: result.to_json
  end
end
