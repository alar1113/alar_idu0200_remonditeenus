class ServiceRequestsController < ApplicationController
  before_action :authenticate_user_account!
  before_action :set_service_request, only: [:show, :edit, :update, :destroy]
  require 'will_paginate/array'

  # GET /service_requests
  # GET /service_requests.json
  def index
    @service_requests = ServiceRequest.includes(:service_request_status_type, employee:[:person], customer: [:person, :enterprise])
    .where(get_where_clause, *get_params)
    .order('service_request_status_type.type_name').paginate(:page => params[:page])
  end

  # GET /service_requests/1
  # GET /service_requests/1.json
  def show
  end

  # GET /service_requests/new
  def new
    @service_request = ServiceRequest.new
  end

  # GET /service_requests/1/edit
  def edit
  end

  # POST /service_requests
  # POST /service_requests.json
  def create
    @service_request = ServiceRequest.new(service_request_params)
    @service_request.created = Time.now
    @service_request.created_by = current_user_account.employee_id

    respond_to do |format|
      if @service_request.save
        format.html { redirect_to edit_service_request_path(@service_request), notice: 'Kliendi pöördumine registreeritud' }
        format.json { render action: 'show', status: :created, location: @service_request }
      else
        format.html { render action: 'new' }
        format.json { render json: @service_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_requests/1
  # PATCH/PUT /service_requests/1.json
  def update
    respond_to do |format|
      if @service_request.update(service_request_params)
        format.html { redirect_to edit_service_request_path(@service_request), notice: 'Kliendi pöördumine salvestatud' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @service_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_requests/1
  # DELETE /service_requests/1.json
  def destroy
    if @service_request.deletable?
      @service_request.destroy
    else
      flash[:error] = 'Pöördumisega on juba seotud tellimused'
    end
    respond_to do |format|
      format.html { redirect_to service_requests_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_request
      @service_request = ServiceRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_request_params
      params.require(:service_request).permit(:service_desc_by_customer, :service_desc_by_employee, :customer_fk, :service_request_status_type_fk)
    end

  def get_params
    [
        customer: "%#{params[:client]}%",
        service_request_status_type_fk: params[:service_request_status_type_fk]
    ]
  end

  def get_where_clause
    where = []
    where << 'service_request_status_type_fk = :service_request_status_type_fk' if has_param?(:service_request_status_type_fk)
    if has_param?(:client)
      where << 'person.last_name ILIKE :customer OR enterprise.name ILIKE :customer'
    end
    where.join(' AND ')
  end

  def has_param?(name)
    !params[name].blank?
  end
end
