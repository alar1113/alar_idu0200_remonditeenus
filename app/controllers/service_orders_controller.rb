class ServiceOrdersController < ApplicationController
  before_action :set_service_order, only: [:show, :edit, :update, :destroy]
  before_action :set_service_request, except: [:delete_service_action]

  # GET /service_orders
  # GET /service_orders.json
  def index
    @service_orders = ServiceOrder.where(service_request_fk: @service_request.id)
    .includes(:so_status_type, creator:[:person], updater:[:person],status_changer:[:person])
    .order('so_status_type.type_name')
  end

  # GET /service_orders/1
  # GET /service_orders/1.json
  def show
  end

  # GET /service_orders/new
  def new
    @service_order = @service_request.service_orders.build
  end

  # GET /service_orders/1/edit
  def edit
    add_empty_sub_objects
  end

  def add_empty_sub_objects
    if @service_order.edit_sub_records?
      @service_order.service_actions.build
      @service_order.service_parts.build
    end
  end

  # POST /service_orders
  # POST /service_orders.json
  def create
    @service_order = ServiceOrder.new(service_order_params)
    @service_order.created = Time.now
    @service_order.created_by = current_user_account.employee_id
    @service_order.so_status_type_fk = SoStatusType::VASTU_VOETUD
    @service_request.service_orders << @service_order

    respond_to do |format|
      if @service_request.save
        format.html { redirect_to edit_service_request_service_order_path(@service_request, @service_order), notice: 'Uus tellimus lisatud' }
        format.json { render action: 'show', status: :created, location: @service_order }
      else
        format.html { render action: 'new' }
        format.json { render json: @service_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_orders/1
  # PATCH/PUT /service_orders/1.json
  def update
    @service_order.updated = Time.now
    @service_order.updated_by = current_user_account.employee_id
    @service_order.attributes = service_order_params

    save_nested_objects

    if @service_order.changed.include?('so_status_type_fk')
      @service_order.status_changed_by = current_user_account.employee_id
      @service_order.status_changed = Time.now
    end
    respond_to do |format|
      if @service_order.save
        format.html { redirect_to edit_service_request_service_order_path(@service_request, @service_order), notice: 'Tellimuse andmed uuendatud' }
        format.json { head :no_content }
      else
        format.html do
          flash.now[:error] = @service_order.errors.to_a
          add_empty_sub_objects
          render action: 'edit'
        end
        format.json { render json: @service_order.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /service_orders/1
  # DELETE /service_orders/1.json
  def destroy
    if @service_order.deletable?
      @service_order.destroy
    else
      flash[:error] = 'Tellimusega on juba seotud teised objektid'
    end
    respond_to do |format|
      format.html { redirect_to service_request_service_orders_url }
      format.json { head :no_content }
    end
  end

  def delete_service_action
    id = params[:id]
    sa = ServiceAction.find_by(service_action: id.to_i)
    so = sa.service_order
    url = edit_service_request_service_order_path(so.service_request, so)
    sa.destroy
    redirect_to url
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_service_order
    @service_order = ServiceOrder.find(params[:id])
  end

  def set_service_request
    @service_request = ServiceRequest.find(params[:service_request_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def service_order_params
    params.require(:service_order).permit(:service_request_fk, :service_order, :so_status_type_fk, :note)
  end

  def separate_sub_objects(form_params, primary_key)
    updatable_objects = []
    new_objects = []
    if params[:service_order] && params[:service_order][form_params]
      params[:service_order][form_params].to_hash.values.each do |zi|
        if zi.key?(primary_key.to_s) && !zi[primary_key.to_s].blank?
          updatable_objects[zi[primary_key.to_s].to_i] = zi
        elsif block_given?
          new_objects << zi if yield(zi)
        else
          new_objects << zi
        end
      end

    end
    {update: updatable_objects, create: new_objects}
  end

  def update_existing_objects(objects, form_objects, primary_key)
    objects.each do |object|
      object.attributes = form_objects[object.send(primary_key)]
    end
  end

  def create_new_objects(clazz, new_objects)
    objects = []
    new_objects.each do |object_hash|
      object = clazz.new(object_hash)
      if object.respond_to?('created')
        object.created = Time.now
      end
      if object.respond_to?('created_by')
        object.created_by = current_user_account.employee_id
      end
      objects << object
    end
    objects
  end

  def save_nested_objects
    service_devices = separate_sub_objects(:service_devices_attributes, :service_device)
    if service_devices[:update]
      update_existing_objects(@service_order.service_devices, service_devices[:update], :service_device)
    end

    service_actions = separate_sub_objects(:service_actions_attributes, :service_action) do |object_hash|
      !object_hash['service_amount'].blank?
    end
    update_existing_objects(@service_order.service_actions, service_actions[:update], :service_action)
    @service_order.service_actions << create_new_objects(ServiceAction, service_actions[:create])


    service_parts = separate_sub_objects(:service_parts_attributes, :service_part) do |object_hash|
      !object_hash['part_count'].blank?
    end
    update_existing_objects(@service_order.service_parts, service_parts[:update], :service_part)
    @service_order.service_parts << create_new_objects(ServicePart, service_parts[:create])
  end
end
